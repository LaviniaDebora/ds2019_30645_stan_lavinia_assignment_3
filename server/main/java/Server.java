import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.*;
import java.util.ArrayList;

public class Server  implements InterfaceRMI {
    private static final long serialVersionUID = 1L;
    Server(){}

    @Override
    public void sendMessage(String s) throws RemoteException {
        System.out.println(s);
    }

    @Override
    public String getMessage(String text) throws RemoteException {
        return "Message: " + text;
    }

    public  ArrayList<Medication> getMedicationPlan() throws RemoteException, SQLException {
        String url;
        Connection myConn = null;
        ArrayList<Medication> list= new ArrayList<Medication>();
        try {
            url="jdbc:mysql://127.0.0.1:3306/mydb?useSSL=false";
            myConn = DriverManager.getConnection(url,"root","starwars");
            Statement myStm=myConn.createStatement();
            ResultSet myRes =myStm.executeQuery("select id from medication_plan where start_date = \"2019-02-01\"");
            int id = 0;

            while (myRes.next())
            {
                id = myRes.getInt(1);
            }

            String sql = "select * from medication where medication_plan_id="+id;
            ResultSet rez = myStm.executeQuery(sql);
            int dosage;
            String start_time;
            String end_time;
            String name;
            Boolean taken;
            int idd;

            while (rez.next())
            {
                idd = rez.getInt("id");
                dosage = rez.getInt("dosage");
                start_time = rez.getString("start_time");
                end_time = rez.getString("end_time");
                name = rez.getString("name");
                taken = rez.getBoolean("taken");
                Medication med = new Medication(idd,name,dosage,start_time,end_time,taken);
                list.add(med);
            }
            myConn.close();

        }
        catch (Exception e) {
            System.out.println(e.toString());
        }
        return list;
    }

    public static void main(String args[]) throws IOException, InterruptedException {
        System.out.println("starting GRPC Server");
        try {
            Server obj = new Server();
            InterfaceRMI stub = (InterfaceRMI) UnicastRemoteObject.exportObject(obj, 0);
            Registry registry = LocateRegistry.createRegistry(8080);
            registry.bind("//localhost:8080/InterfaceRMI",  stub);
            registry.rebind("//localhost:8080/InterfaceRMI", stub);
            System.err.println("Server ready");
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }
}
