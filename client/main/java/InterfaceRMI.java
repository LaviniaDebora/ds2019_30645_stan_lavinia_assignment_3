import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface InterfaceRMI extends Remote{

   ArrayList<Medication> getMedicationPlan()throws RemoteException;

   public void sendMessage(String text) throws RemoteException;

   public String getMessage(String text) throws RemoteException;
}
