import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.time.LocalTime;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Client {

    private static final long serialVersionUID = 1L;

    public Client() {
    }

    public static <TableButton, TableButtonListener> void main(String[] args) throws RemoteException {

        ArrayList<Medication> list= new ArrayList<Medication>();
        InterfaceRMI stub = null;

        try {
            Registry registry = LocateRegistry.getRegistry(8080);
             stub = (InterfaceRMI) registry.lookup("//localhost:8080/InterfaceRMI");
            list = stub.getMedicationPlan();
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }

        LocalTime localTime = LocalTime.now();
        System.out.println(localTime.toString());
        String predefined="00:00:00";
        if(predefined.equals(localTime.toString())){
            list = stub.getMedicationPlan();
        }

        JFrame frame = new JFrame("PILL Dispatcher");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Object[] columns = {"Name","Dosage","Start","End", "Taken"};
        JTable table = new JTable(){

            public Class getColumnClass(int column)
            {
                return getValueAt(0, column).getClass();
            }
        };

        DefaultTableModel model = new DefaultTableModel(){
            public Class<?> getColumnClass(int column) {
                switch (column) {
                    case 0: return String.class;
                    case 1: return String.class;
                    case 2: return String.class;
                    case 3: return String.class;
                    case 4: return Boolean.class;
                    default:return String.class;
                }
            }
        };

        model.setColumnIdentifiers(columns);
        table.setModel(model);
        table.setForeground(Color.black);
        Font font = new Font("",1,18);
        table.setFont(font);
        table.setRowHeight(30);

        JButton btnAdd = new JButton("View");
        JButton btnStart = new JButton("Start");
        JButton btncheck = new JButton("Check");
        JLabel label = new JLabel();
        
        label.setBounds(300, 310, 200, 30);
        btnAdd.setBounds(300, 220, 100, 30);
        btnStart.setBounds(300, 320, 100, 30);
        btncheck.setBounds(300, 320, 100, 30);

        JScrollPane pane = new JScrollPane(table);
        pane.setBounds(0, 0, 700, 200);

        JPanel panel = new JPanel(new FlowLayout());
        panel.add(btnAdd);
        panel.add(btncheck);

        frame.setLayout(new FlowLayout());
        frame.add(pane);
        frame.add(panel);
        frame.add(new TimeDisplay());
        frame.pack();

        Object[] row = new Object[5];
        ArrayList<Medication> finalList = list;

        btnAdd.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {

                finalList.forEach(x->{
                            row[0] = x.getName();
                            row[1] = x.getDosage();
                            row[2] = x.getStartTime();
                            row[3] = x.getEndTime();
                            row[4]="Take";
                            model.addRow(row);
                        }
                );

            }
        });

        btncheck.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {

                int i = table.getRowCount();
                if(i>0) {
                    try {
                        Registry registry = LocateRegistry.getRegistry(8080);
                        InterfaceRMI stub = (InterfaceRMI) registry.lookup("//localhost:8080/InterfaceRMI");
                        if (stub != null) {
                            stub.sendMessage("# Notification: Medications not taken!");
                            System.out.println(stub.getMessage("# Notification: Medications not taken!"));
                        }

                    } catch (RemoteException | NotBoundException ex) {
                        ex.printStackTrace();
                    }
                }

            }
        });

        TableDisplay buttonColumn = new TableDisplay(table, 4);

        frame.setSize(790,500);
        frame.setVisible(true);
    }
}
