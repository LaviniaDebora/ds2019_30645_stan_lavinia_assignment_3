import javax.swing.*;
import java.awt.event.ActionListener;
import java.rmi.NotBoundException;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import java.awt.*;
import java.awt.event.ActionEvent;


class TableDisplay extends AbstractCellEditor implements ActionListener, TableCellRenderer, TableCellEditor
{

    JButton renderButton;
    String text;
    JButton editButton;
    JTable table;

    public TableDisplay(JTable table, int column)
    {
        super();

        TableColumnModel columnModel = table.getColumnModel();
        columnModel.getColumn(column).setCellRenderer( this );
        columnModel.getColumn(column).setCellEditor( this );

        this.table = table;
        renderButton = new JButton();

        editButton = new JButton();
        editButton.setFocusPainted( false );
        editButton.addActionListener( this );

    }

    public Component getTableCellRendererComponent(
            JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
        if (hasFocus)
        {
            renderButton.setForeground(table.getForeground());
            renderButton.setBackground(UIManager.getColor("Button.background"));
        }
        else if (isSelected)
        {
            renderButton.setForeground(table.getSelectionForeground());
            renderButton.setBackground(table.getSelectionBackground());
        }
        else
        {
            renderButton.setForeground(table.getForeground());
            renderButton.setBackground(UIManager.getColor("Button.background"));
        }

        renderButton.setText( (value == null) ? "" : value.toString() );
        return renderButton;
    }

    public Component getTableCellEditorComponent(
            JTable table, Object value, boolean isSelected, int row, int column)
    {
        text = (value == null) ? "" : value.toString();
        editButton.setText( text );
        return editButton;
    }

    public Object getCellEditorValue()
    {
        return text;
    }

    public void actionPerformed(ActionEvent e)
    {
        fireEditingStopped();
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        int i = table.getSelectedRow();
        if(i >= 0){
            model.removeRow(i);
            try {
                Registry registry = LocateRegistry.getRegistry(8080);
                InterfaceRMI stub = (InterfaceRMI) registry.lookup("//localhost:8080/InterfaceRMI");
                if (stub != null) {
                    stub.sendMessage("# Notification: Medication taken!");
                    System.out.println(stub.getMessage("# Notification: Medication taken!"));
                }

            } catch (RemoteException | NotBoundException ex) {
                ex.printStackTrace();
            }
        }
        else{
            System.out.println(" Error");
        }
    }

}
